TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp \
    polygon_2d.cpp \
    polygon_3d.cpp \
    gl.cpp

HEADERS += \
    vector_2d.h \
    vector_3d.h \
    line_3d.h \
    gl.h \
    polygon_2d.h \
    polygon_3d.h

