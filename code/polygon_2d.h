#ifndef POLYGON_2D_H
#define POLYGON_2D_H

#include "gl.h"
#include "vector_2d.h"

class polygon_2d
{
public:
  polygon_2d()
  {
    vertexes = 0;
    size = 0;
    type = gl::INDEFINITE;
  }

  // Список вершин, его размерность и тип многоугольника
  vector_2d *vertexes;
  int size;
  gl::polygon_type type;

};

#endif // POLYGON_2D_H
