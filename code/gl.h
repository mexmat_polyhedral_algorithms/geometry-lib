#ifndef GL_H
#define GL_H

class gl
{
  public:
  gl();
  ~gl();

  static double EPS;

  enum polygon_type
  {
    INDEFINITE,
    CONVEX,
    CONCAVE
  };
};

#endif // GL_H
