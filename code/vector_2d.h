#ifndef VECTOR_2D_H
#define VECTOR_2D_H

#include <math.h>

#include "gl.h"

class vector_2d
{
public:
  // Координаты вектора
  double x, y;

  // Конструкторы

  // Пустой конструктор
  vector_2d() : x(0.0), y(0.0) {}

  // Конструктор, с инициализацией заданными значениями
  vector_2d(double a, double b) : x(a), y(b) {}

  // Деструктор
  ~vector_2d() {}

  // Перегрузка операторов

  // Унарные операции

  // Унарное сложение вектора
  vector_2d &operator +=(const vector_2d &vector)
  {
    x += vector.x;
    y += vector.y;
    return *this;
  }

  // Унарное вычитание векторов
  vector_2d &operator -=(const vector_2d &vector)
  {
    x -= vector.x;
    y -= vector.y;
    return *this;
  }

  // Унарное умножение вектора на вещественное число
  vector_2d &operator *=(const double &number)
  {
    x *= number;
    y *= number;
    return *this;
  }

  // Унарное деление на вещественное число. Нет проверки деления на 0!
  vector_2d &operator /=(const double &number)
  {
    x /= number;
    y /= number;
    return *this;
  }

  // Проверка вектора на равенство нулю. true, если каждая координата не отличима от нуля на точность gl::EPS.
  bool operator !() const
  {
    return fabs(x) < gl::EPS && fabs(y) < gl::EPS;
  }

  // Унарное умножение вектора на (-1)
  vector_2d operator -() const
  {
    return vector_2d(-x, -y);
  }

  // Бинарные операции

  // Бинарное сложение
  friend inline const vector_2d operator +(const vector_2d &left_vector, const vector_2d &right_vector);
  // Бинарное вычитание
  friend inline const vector_2d operator -(const vector_2d &left_vector, const vector_2d &right_vector);
  // Умножение вектора на число справа
  friend inline const vector_2d operator *(const vector_2d &vector, const double &number);
  // Умножение вектора на число слева
  friend inline const vector_2d operator *(const double &number, const vector_2d &vector);
  // Деление вектора на число. Проверка деления на 0 не производится!
  friend inline const vector_2d operator /(const vector_2d &vector, const double &number);
  // Скалярное произведение двух векторов
  friend inline double operator *(const vector_2d &left_vector, const vector_2d &right_vector);
  // Векторное произведение
  friend inline double operator %(const vector_2d &left_vector, const vector_2d &right_vector);
  // Операция неравно для векторов. true, если векторы не равны. Сравнение идёт с точностью gl::EPS.
  friend inline bool operator !=(const vector_2d &left_vector, const vector_2d &right_vector);
  // Операция проверки на равенство двух векторов. Сравнение идёт с точностью gl::EPS.
  friend inline bool operator ==(const vector_2d &left_vector, const vector_2d &right_vector);


  // Методы

  // Обнуление вектора
  void null()
  {
    x = 0.0;
    y = 0.0;
  }

  // Нахождение левого перпендикуляра к вектору
  vector_2d left_perpendicular() const
  {
    return vector_2d(-y, x);
  }

  // Нахождение правого перпендикуляра к вектору
  vector_2d right_perpendicular() const
  {
    return vector_2d(y, -x);
  }

  // Норма вектора
  double length() const
  {
    return sqrt(x * x + y * y);
  }

  // Манхэттенская норма
  double manhattan_norm() const
  {
    return x * x + y * y;
  }

  // Нормирование вектора
  void norm()
  {
    double norm = this->length ();
    x /= norm;
    y /= norm;
  }
};

// Бинарные операторы

// Бинарное сложение
inline const vector_2d operator +(const vector_2d &left_vector, const vector_2d &right_vector)
{
  return vector_2d(left_vector.x + right_vector.x, left_vector.y + right_vector.y);
}

// Бинарное вычитание
inline const vector_2d operator -(const vector_2d &left_vector, const vector_2d &right_vector)
{
  return vector_2d(left_vector.x - right_vector.x, left_vector.y - right_vector.y);
}

// Умножение вектора на число справа
inline const vector_2d operator *(const vector_2d &vector, const double &number)
{
  return vector_2d(number * vector.x, number * vector.y);
}

// Умножение на число слева
inline const vector_2d operator *(const double &number, const vector_2d &vector)
{
  return vector_2d(number * vector.x, number * vector.y);
}

// Деление вектора на число. Проверка деления на 0 не производится!
inline const vector_2d operator /(const vector_2d &vector, const double &number)
{
  return vector_2d(vector.x / number, vector.y / number);
}

// Скалярное произведение двух векторов
inline double operator *(const vector_2d &left_vector, const vector_2d &right_vector)
{
  return left_vector.x * right_vector.x + left_vector.y * right_vector.y;
}

// Векторное произведение
inline double operator %(const vector_2d &left_vector, const vector_2d &right_vector)
{
  return left_vector.x * right_vector.y - right_vector.x * left_vector.y;
}

// Операция неравно для векторов. true, если векторы не равны.
inline bool operator !=(const vector_2d &left_vector, const vector_2d &right_vector)
{
  return fabs(right_vector.x - left_vector.x) > gl::EPS || fabs(right_vector.y - left_vector.y) > gl::EPS;
}

// Операция проверки на равенство двух векторов
inline bool operator ==(const vector_2d &left_vector, const vector_2d &right_vector)
{
  return fabs(right_vector.x - left_vector.x) < gl::EPS && fabs(right_vector.y - left_vector.y) < gl::EPS;
}

#endif // VECTOR_2D_H
