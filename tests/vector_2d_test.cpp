#include "vector_2d_test.h"

unsigned int vector_2d_test(const char *dir)
{
  unsigned int failure_test_counter = 0;
  bool test_result;

  char buffer[MAX_BUFF_LENGHT];

  sprintf(buffer, "%s%s", dir, "/vector_2d_report.log");

  reporter report(buffer, "Vector_2d class");

  // Тест на простую инициализацию вектора
  vector_2d first_test_vector;

  if(fabs(first_test_vector.x) < MINIMAL_FOR_COMPARE && fabs(first_test_vector.y) < MINIMAL_FOR_COMPARE)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Simple initialization vector", test_result);

  // Проверка на инициализацию вектора с заданными значениями
  vector_2d second_test_vector(1.0, 1.0);

  if(fabs(second_test_vector.x - 1.0) < MINIMAL_FOR_COMPARE && fabs(second_test_vector.y - 1.0) < MINIMAL_FOR_COMPARE)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Initialization vector with adjusted values", test_result);

  // Унарные операции
  // Унарное сложение векторов
  first_test_vector += second_test_vector;

  if(fabs(first_test_vector.x - 1.0) < MINIMAL_FOR_COMPARE && fabs(first_test_vector.y - 1.0) < MINIMAL_FOR_COMPARE)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Unary summation of vectors", test_result);

  // Унарное вычитание векторов
  first_test_vector.x = 1.0;
  first_test_vector.y = 1.0;

  first_test_vector -= second_test_vector;

  if(fabs(first_test_vector.x) < MINIMAL_FOR_COMPARE && fabs(first_test_vector.y) < MINIMAL_FOR_COMPARE)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Unary subtract vectors", test_result);

  // Унарное умножение вектора на вещественное число
  first_test_vector.x = 1.0;
  first_test_vector.y = -1.0;

  first_test_vector *= 2.0;

  if(fabs(first_test_vector.x - 2.0) < MINIMAL_FOR_COMPARE && fabs(first_test_vector.y + 2.0) < MINIMAL_FOR_COMPARE)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Unary multiplication of a vector by a real number", test_result);

  // Унарное деление на вещественное число
  first_test_vector.x = 6.0;
  first_test_vector.y = -3.0;

  first_test_vector /= 3.0;

  if(fabs(first_test_vector.x - 2.0) < MINIMAL_FOR_COMPARE && fabs(first_test_vector.y + 1.0) < MINIMAL_FOR_COMPARE)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Unary division of the real number", test_result);

  // Проверка вектора на равенство нулю
  first_test_vector.x = gl::EPS - (gl::EPS / 10.0);
  first_test_vector.y = (-1) * gl::EPS + (gl::EPS / 100.0);

  second_test_vector.x = gl::EPS + (gl::EPS / 10.0);
  second_test_vector.y = (-1) * gl::EPS - (gl::EPS / 100.0);

  if(!first_test_vector && !!second_test_vector)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Check vector on the equality to zero", test_result);

  // Унарное умножение вектора на (-1)
  first_test_vector.x = 1.0;
  first_test_vector.y = -1.0;

  second_test_vector = -first_test_vector;

  if(fabs(second_test_vector.x + 1.0) < MINIMAL_FOR_COMPARE && fabs(second_test_vector.y - 1.0) < MINIMAL_FOR_COMPARE)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Unary multiplication of a vector by (-1)", test_result);

  // Бинарные операции

  // Бинарное сложение
  vector_2d third_test_vector;

  first_test_vector.x = 1.0;
  first_test_vector.y = -1.0;
  second_test_vector.x = 3.0;
  second_test_vector.y = -3.0;

  third_test_vector = first_test_vector + second_test_vector;

  if(fabs(third_test_vector.x - 4.0) < MINIMAL_FOR_COMPARE && fabs(third_test_vector.y + 4.0) < MINIMAL_FOR_COMPARE)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Binary addition", test_result);

  // Бинарное вычитание
  first_test_vector.x = 1.0;
  first_test_vector.y = -2.0;
  second_test_vector.x = 1.0;
  second_test_vector.y = -2.0;

  third_test_vector = first_test_vector - second_test_vector;

  if(fabs(third_test_vector.x) < MINIMAL_FOR_COMPARE && fabs(third_test_vector.y) < MINIMAL_FOR_COMPARE)
    test_result = true;
  else
    test_result = false;

  report.print_test_report("Binary subtraction", test_result);

  // Умножение вектора на число справа
  first_test_vector.x = 1.0;
  first_test_vector.y = -1.0;

  third_test_vector = first_test_vector * 3.0;

  if(fabs(third_test_vector.x - 3.0) < MINIMAL_FOR_COMPARE && fabs(third_test_vector.y + 3.0) < MINIMAL_FOR_COMPARE)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Multiplication of a vector by a number on the right", test_result);

  // Умножение вектора на число слева
  first_test_vector.x = 1.0;
  first_test_vector.y = -1.0;

  third_test_vector = 3.0 * first_test_vector;

  if(fabs(third_test_vector.x - 3.0) < MINIMAL_FOR_COMPARE && fabs(third_test_vector.y + 3.0) < MINIMAL_FOR_COMPARE)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Multiplication of a vector by a number left", test_result);

  // Деление вектора на число
  first_test_vector.x = 10.0;
  first_test_vector.y = -5.0;

  third_test_vector = first_test_vector / 5.0;

  if(fabs(third_test_vector.x - 2.0) < MINIMAL_FOR_COMPARE && fabs(third_test_vector.y + 1.0) < MINIMAL_FOR_COMPARE)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Division of vector by a number", test_result);

  // Скалярное произведение двух векторов
  vector_2d fourth_test_vector, fifth_test_vector, sixth_test_vector;

  first_test_vector.x = 1.0;
  first_test_vector.y = 0.0;

  second_test_vector.x = 0.0;
  second_test_vector.y = 1.0;

  double res_1 = first_test_vector * second_test_vector;

  third_test_vector.x = 1.0;
  third_test_vector.y = -5.0;

  fourth_test_vector.x = 6.0;
  fourth_test_vector.y = 2.0;

  double res_2 = third_test_vector * fourth_test_vector;

  fifth_test_vector.x = 6.0;
  fifth_test_vector.y = 2.0;

  sixth_test_vector.x =  -3.0;
  sixth_test_vector.y = 5.0;

  double res_3 = fifth_test_vector * sixth_test_vector;

  if(fabs(res_1) < MINIMAL_FOR_COMPARE && fabs(res_2 + 4.0) < MINIMAL_FOR_COMPARE && fabs(res_3 + 8.0) < MINIMAL_FOR_COMPARE)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Scalar product", test_result);

  // Векторное произведение
  first_test_vector.x = 1.0;
  first_test_vector.y = -1.0;

  second_test_vector.x = -1.0;
  second_test_vector.y = 1.0;

  res_1 = first_test_vector % second_test_vector;

  first_test_vector.x = 1.0;
  first_test_vector.y = 2.0;

  second_test_vector.x = 2.0;
  second_test_vector.y = 3.0;

  res_2 = first_test_vector % second_test_vector;

  if(fabs(res_1) < MINIMAL_FOR_COMPARE && fabs(res_2 + 1.0) < MINIMAL_FOR_COMPARE)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Crossproduct", test_result);

  // Операция неравно для векторов
  first_test_vector.x = 1.0;
  first_test_vector.y = -1.0;

  second_test_vector.x = 1.0;
  second_test_vector.y = -1.0;

  third_test_vector.x = 2.0;
  third_test_vector.y = -3.0;

  if(!(first_test_vector != second_test_vector) && second_test_vector != third_test_vector)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Operation unequally vectors", test_result);

  // Операция проверки на равенство двух векторов
  first_test_vector.x = 1.0;
  first_test_vector.y = -1.0;

  second_test_vector.x = 1.0;
  second_test_vector.y = -1.0;

  third_test_vector.x = 2.0;
  third_test_vector.y = -3.0;

  if(first_test_vector == second_test_vector && !(first_test_vector == third_test_vector))
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Operation to test the equality of two vectors", test_result);

  // Методы

  // Обнуление вектора
  first_test_vector.x = 5.0;
  first_test_vector.y = -1.0;

  first_test_vector.null();

  if(fabs(first_test_vector.x) < MINIMAL_FOR_COMPARE && fabs(first_test_vector.y) < MINIMAL_FOR_COMPARE)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Zeroing vector", test_result);

  // Нормирование вектора
  first_test_vector.x = 5.0;
  first_test_vector.y = -10.0;

  first_test_vector.norm();

  if(fabs(sqrt(first_test_vector.x * first_test_vector.x + first_test_vector.y * first_test_vector.y) - 1.0) < MINIMAL_FOR_COMPARE)
    test_result = true;
  else
    test_result = false;

  if(!test_result)
    ++failure_test_counter;

  report.print_test_report("Normalization vector", test_result);


  return failure_test_counter;
}
