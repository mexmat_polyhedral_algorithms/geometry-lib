TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp \
    vector_2d_test.cpp \
    reporter.cpp \
    ../code/gl.cpp \
    ../code/polygon_3d.cpp \
    ../code/polygon_2d.cpp

HEADERS += \
    vector_2d_test.h \
    reporter.h \
    const.h \
    ../code/line_3d.h \
    ../code/gl.h \
    ../code/vector_3d.h \
    ../code/vector_2d.h \
    ../code/polygon_3d.h \
    ../code/polygon_2d.h

