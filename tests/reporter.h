#ifndef REPORTER_H
#define REPORTER_H

#include <stdio.h>

// Класс создания отчёта о тестировании
class reporter
{
public:
  // Конструктор
  reporter(const char *filename, const char *_report_name);

  // Деструктор
  ~reporter();

  // Функция записи сообщения о тесте
  void print_test_report(const char *message, bool status);

  // Функция возвращение статуса системы (функционирует нормально или нет, скажем не открылся файл для записи отчёта)
  bool get_system_status();

private:
  const char *report_name;
  FILE *fd;
  bool system_status;

  // Функция печати на экран
  void print_to_display(const char *message, bool status);

  // Функция печати в файл
  void print_to_file(const char *message, bool status);
};

#endif // REPORTER_H
