#include "reporter.h"

// Конструктор
reporter::reporter(const char *filename, const char *_report_name) : report_name(_report_name)
{
  fd = fopen(filename, "w");

  if(fd == NULL)
    system_status = false;
  else
    {
      system_status = true;
      fprintf(fd, "%s test report\n\n\n", report_name);
    }
}

// Деструктор
reporter::~reporter()
{
  fclose(fd);
}

// Функция записи сообщения о тесте
void reporter::print_test_report(const char *message, bool status)
{
  if(system_status)
    {
      print_to_display(message, status);
      print_to_file(message, status);
    }
  else
    {
      print_to_display(message, status);
    }
}

// Функция печати на экран
void reporter::print_to_display(const char *message, bool status)
{
  if(status)
    printf("%s \t\t OK\n", message);
  else
    printf("%s \t\t FAIL\n", message);
}

// Функция печати в файл
void reporter::print_to_file(const char *message, bool status)
{
  if(status)
    fprintf(fd, "%s \t\t OK\n", message);
  else
    fprintf(fd, "%s \t\t FAIL\n", message);
}
