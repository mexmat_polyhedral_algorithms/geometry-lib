#ifndef VECTOR_2D_TEST_H
#define VECTOR_2D_TEST_H

#include <iostream>
#include <math.h>

#include "const.h"
#include "reporter.h"

#include "../code/vector_2d.h"

// Функция тестирования класса vector_2d
unsigned int vector_2d_test(const char *dir);

#endif // VECTOR_2D_TEST_H
